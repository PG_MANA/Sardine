# Sardine
(Format:UTF-8)  
LinuxデスクトップユーザのためのサイボウズLive 新着情報受信アプリ
## 概要
Linux/X11を対象としたサイボウズLive 新着情報受信アプリです。と言ってもQtを使用しているので環境さえ整えばMacやWindowsでも動作すると思います。  
なおQtオープンソース版のLGPLv3を選択しています。 Qtのライセンスは https://www.qt.io/licensing/ をご覧ください。  
自分の作ってたライブラリなどのコードを一部取ってきてササッと数日で改造したので、メモリ使用量・コード最適化がしっかりできてないと思います。  
KDevelop 5で開発していますが、使用しなくてもビルドはできます。  
openSUSE Tumbleweed で開発してます。  
画面に表示されてる各文章をダブルクリックすると該当ページをブラウザで表示します。  
また起動オプションに「-n」をつけると起動時にウィンドウを表示しません。起動時の自動実行などでご利用ください。  
### 動作環境
* Qt 5.9

ブラウザが快適に動作する程度ならこのソフトウェアも快適に動作するはずです。
## ライセンス
Copyright 2017 PG_MANA  
This software is Licensed under the Apache License Version 2.0  
See LICENSE.md  
注意:src/Resources/icon/以下のアイコンなどは「ロゴ」にあたり、派生成果物に含めることはできません。  
補足:src/Network/SHA1cc.cppとsrc/Network/SHA1cc.hはパブリックドメイン実装の https://ja.osdn.net/projects/sha1cc/ の派生物です。  
## ビルド
### ビルド環境
* Qt 5.9 (Qt5::Widgets Qt5::Network) 開発用ヘッダファイルなど
* c++17対応C++コンパイラ
* cmake  3.1以上

### ビルド方法

```shell
cd プロジェクトディレクトリ  
mkdir build  
cd build  
cmake ../  
make  
./sardine
```

### rpmビルド方法

```shell
#rpmbuildディレクトリがhomeディレクトリにあることを前提としてます。  
./make-rpm.sh  
#成功すると~/rpmbuild/RPMS/(各Arch)/にrpmができてると思います。  
#なお、make-rpm.shは引数をrpmbuildにすべて投げるので署名したい場合は
./make-rpm.sh --sign   #とすると署名されます。
```

### ビルドエラーが出た時
* 「Please set the CONS_KEY and CONS_SEC」と出た => src/Key.hに https://developer.cybozulive.com より取得してきたCONS_KEY CONS_SECを書き込み、「#error  Please set the CONS_KEY and CONS_SEC」の行を削除してください。
* 「_rotlが定義されていない」と出た => src/Network/Sha1cc.hに「#define NOT_ROL」という風にNOT_ROLを定義してください。

## ビルド済みバイナリ(rpm)
* https://repo.taprix.org/pg_mana/linux/rpm/x86_64/Sardine/

## 備考
* Gitリポジトリに上げる際、Key.hをGit管理対象から外すなどしたほうが良いです。

## リンク
### Qtのドキュメントページ
  https://doc.qt.io/
### 開発者のTwitterアカウント
  [https://twitter.com/PG_MANA_](https://twitter.com/PG_MANA_)
### 開発者のWebページ
  https://pg-mana.net
