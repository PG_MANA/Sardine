/*
 * Copyright 2017 PG_MANA
 *
 * This software is Licensed under the Apache License Version 2.0
 * See LICENSE.md
 *
 * Sardine
 *
 * Qtを使用したCybozulive用クライアント
 */
#include "Sardine.h"
#include "UI/MainWindow.h"
#include <QApplication>
#include <QTextCodec>

int main ( int argc, char *argv[] ) {
    QApplication app ( argc, argv );
    //全般設定
    QTextCodec::setCodecForLocale ( QTextCodec::codecForName ( "UTF-8" ) );
    app.setWindowIcon ( QIcon ( ":/icon-normal.png" ) ); //埋め込み
    bool show_window = true;
    for ( int cnt = 1; cnt <argc; cnt++ ) {
        if( argv[cnt][0] == '-' && argv[cnt][1] == 'n' && argv[cnt][2] == '\0' ) { //スーパー古典的比較
            show_window = false;
            break;
        }
    }
    MainWindow window;
    return window.init ( "default.ini" ) ?((show_window)?window.show():[] {}()),app.exec() :EXIT_FAILURE;
}
