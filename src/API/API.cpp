/*
 * Copyright 2017 PG_MANA
 *
 * This software is Licensed under the Apache License Version 2.0
 * See LICENSE.md
 *
 * Cybozulive通信部分
 * 接続自体はNetworkクラスに任せる。
 */
#include "API.h"
#include "Setting.h"
#include "CybozuUrl.h"
#include "../Network/Network.h"
#include "../Network/OAuth.h"
#include "../Key.h"
#include <QtCore>
#include <QNetworkRequest>
#include <QNetworkReply>

API::API ( Setting *setting ) {
    if ( setting ) {
        oauth = new OAuth ( CONS_KEY, CONS_SEC, setting->oauth_token, setting->oauth_secret );
    } else {
        oauth = nullptr;
    }
}

API::~API() {
    delete oauth;
}

/*
 * 引数:oauth_token,oauth_token_secret,,oauth_verifier(PINコード)
 * 戻値:受信用QNetworkReply
 * 概要:アクセストークンの取得を行う。初期設定時以外には呼ばない。
 */
QNetworkReply *API::access_token ( const char *oauth_token,const char *oauth_token_secret,const char *oauth_verifier ) {
    OAuth pr_oauth ( CONS_KEY, CONS_SEC, oauth_token, oauth_token_secret );
    std::vector<OAuth::entry> ele;
    std::string str;
    QNetworkRequest request;

    //ヘッダー生成
    ele.push_back ( { "oauth_verifier",oauth_verifier,false} );
    pr_oauth.makeOAuthHeader ( CybozuUrl::access_token, false, ele, str );
    request.setUrl ( QUrl ( CybozuUrl::access_token ) );
    request.setRawHeader ( "Authorization",str.c_str() );
    return net.get ( request );
}

/*
 * 引数:token_key(トークンキー)
 * 戻値:ブラウザで表示するURL
 * 概要:認証ページのURLを生成する。
 */
QUrl API::authorize_url ( const QString& token ) {
    return QUrl ( CybozuUrl::authorize + token );
}

/*
 * 引数:なし
 * 戻値:postしたあとのQNetworkReply
 * 概要:リクエストトークンの取得を行う。初期設定時以外には呼ばない。
 */
QNetworkReply * API::request_token() {
    OAuth pr_oauth ( CONS_KEY, CONS_SEC, "", "" ); //トークン見所持
    std::vector<OAuth::entry> ele;
    std::string str;
    QNetworkRequest request;

    //ヘッダー生成
    pr_oauth.makeOAuthHeader ( CybozuUrl::request_token, true, ele,str );
    request.setUrl ( QUrl ( CybozuUrl::request_token ) );
    request.setRawHeader ( "Authorization",str.c_str() );
    return net.post ( request,QByteArray() ); //Post内容は空
}

/*
 * 引数:data(access_token関数の実行後のレスポンス),set(設定格納先)
 * 戻値:APISettings
 * 概要:各種トークンなどのの分析。初期設定時以外には呼ばない。またOAuthクラスの作成もこの関数でされる。
 */
void API::decode_access_token ( const QByteArray &data,Setting &set ) {
    QList<QByteArray> list = data.split ( '&' );
    QByteArray  token_key = list[0].split ( '=' ) [1]; //oauth_tokenが、前に入っているという大前提の依存
    QByteArray  token_sec = list[1].split ( '=' ) [1]; //上と同じく拡張性が低い

    strcpy ( set.oauth_token, token_key.constData() );
    strcpy ( set.oauth_secret, token_sec.constData() );
    /* strcpy ( set.user_id, list[2].split ( '=' ) [1].constData() );
     strcpy ( set.user_name, list[3].split ( '=' ) [1].constData() );;*/
    oauth = new OAuth ( CONS_KEY, CONS_SEC, token_key.constData(), token_sec.constData() );
    return;
}

/*
 * 引数:なし
 * 戻値:getしたあとのQNetworkReply
 * 概要:通知の取得を行う、通知はXMLで帰ってくる
 */
QNetworkReply* API::notification_v2 ( bool unconfirmed ) {
    QNetworkRequest request;
    std::vector<OAuth::entry> ele;
    std::string str;

    if ( unconfirmed ) ele.push_back ( {"unconfirmed","true",true} );
    oauth->makeOAuthHeader ( CybozuUrl::notification_v2, false, ele,str );
    request.setUrl ( QUrl ( CybozuUrl::notification_v2 + QString ( ( unconfirmed ) ?"?unconfirmed=true":"" ) ) );
    request.setRawHeader ( "Authorization",str.c_str() );
    return net.get ( request );
}
