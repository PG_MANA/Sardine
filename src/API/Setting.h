/*
 * Copyright 2017 PG_MANA
 *
 * This software is Licensed under the Apache License Version 2.0
 * See LICENSE.md
 *
 * 設定を保存&読込する構造体
 */
#pragma once

#include <QSettings>

struct Setting {
    char oauth_token[64];
    char oauth_secret[64];

    explicit Setting ( const QString &file_name );

    bool save ();
    bool isEmpty();
    //おまけ
    QByteArray geometry();
    void setGeometry ( const QByteArray &geometry );
    int interval();
    void setInterval(int min);
private:
    //暗号化
    bool encode_string ( char *str );
    bool decode_string ( char *str );
    //その他
    QString getFilePath ( const QString &file_name );
    QSettings setting;
};

