/*
 * Copyright 2017 PG_MANA
 *
 * This software is Licensed under the Apache License Version 2.0
 * See LICENSE.md
 *
 * CybozuLiveのAPIのUrlをおいておく
 */

//classではないほうがいいかと...
namespace CybozuUrl {
static const char cybozulive_main_page[] = "https://cybozulive.com/";

//認証関係
static const char access_token[] = "https://api.cybozulive.com/oauth/token";
static const char request_token[] = "https://api.cybozulive.com/oauth/initiate";
static const char authorize[] =  "https://api.cybozulive.com/oauth/authorize?oauth_token=";

//更新情報
static const char notification_v2[] = "https://api.cybozulive.com/api/notification/V2";
}

