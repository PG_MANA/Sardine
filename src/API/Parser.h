/*
 * Copyright 2017 PG_MANA
 *
 * This software is Licensed under the Apache License Version 2.0
 * See LICENSE.md
 *
 * XML解析クラス(CybozuLive終了を受けて通知しか対応しない)
 */
#pragma once

#include <QString>
#include <QVector>

class QDomDocument;

struct NotificationData{
    QString title;
    QString link;
    QString author;
    QString text;
};

class Parser{
public:
    explicit Parser(const QString &string);
    virtual ~Parser();
    QVector<NotificationData> getNotification();
private:
    QDomDocument *xml;
};
