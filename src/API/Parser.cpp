/*
 * Copyright 2017 PG_MANA
 *
 * This software is Licensed under the Apache License Version 2.0
 * See LICENSE.md
 *
 * XML解析クラス(CybozuLive終了を受けて通知しか対応しない)
 */
#include "Parser.h"
#include "CybozuUrl.h"
#include <QDomDocument>
#include <QDebug>

Parser::Parser ( const QString& string ) {
    xml = new QDomDocument;
    xml->setContent ( string );
}

Parser::~Parser() {
    delete xml;
}

/*
 * 引数:なし
 * 戻値:NotificationDataの配列
 * 概要:XMLを解析してNotifcationDataを生成する
 */
QVector<NotificationData> Parser::getNotification() {
    QVector<NotificationData> result;
    QString &&my_id = xml->elementsByTagName ( "author" ).at ( 0 ).namedItem ( "uri" ).toElement().text();
    QDomNodeList notification_list = xml->elementsByTagName ( "entry" );

    for ( int cnt = 0,len = notification_list.length() ; cnt < len ; cnt++ ) {
        QDomNode entry = notification_list.at ( cnt );
        QString &&author_id = entry.namedItem ( "author" ).namedItem ( "uri" ).toElement().text();
        if ( author_id == my_id ) continue;

        result.append ( {/*title*/entry.namedItem ( "title" ).toElement().text(),
                                  /*link(PCが先のはず...)*/entry.namedItem ( "link" ).toElement().attribute ( "href",CybozuUrl::cybozulive_main_page ),
                                  /*author*/entry.namedItem ( "author" ).namedItem ( "name" ).toElement().text(),
                                  /*text*/entry.namedItem ( "summary" ).toElement().text(),
                        } );
    }
    return result;
}
