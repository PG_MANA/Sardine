/*
 * Copyright 2017 PG_MANA
 *
 * This software is Licensed under the Apache License Version 2.0
 * See LICENSE.md
 *
 * Cybozulive通信部分
 */
#pragma once

#include "../Network/Network.h"
#include "../Network/OAuth.h"

class  QNetworkReply;
class QNetworkRequest;
class QByteArray;
class QSettings;

struct Setting;

class API {
public:
    explicit API ( Setting *set = nullptr );
    virtual ~API();
    static API *fromSetting ( QString ini_file_name );

    //認証関係
    QNetworkReply* request_token();
    QUrl authorize_url(const QString &token);
    QNetworkReply* access_token ( const char *oauth_token,const char *oauth_token_secret,const char *oauth_verifier );
    void decode_access_token ( const QByteArray &data,Setting &set );

    //入出力関係
    bool save ( Setting setting ,QString ini_file_name ) ;

    //通知
    QNetworkReply* notification_v2(bool unconfirmed = false);
private:
    static bool encode_setting ( Setting set/*注意(ポインタじゃない)*/, QString &filepath );
    static bool decode_setting ( Setting &set, QSettings &setting );
    static bool encode_string ( char *str );
    static bool decode_string ( char *str );

    OAuth *oauth;
    Network net;
};
