/*
 * Copyright 2017 PG_MANA
 *
 * This software is Licensed under the Apache License Version 2.0
 * See LICENSE.md
 *
 * メインウィンドウ クラス
 * 主役である画面の表示を請け負っている。
 */
#pragma once

#include <QMainWindow>
#include <QString>
#include <QSystemTrayIcon>
#include <QTimer>

class API;
class QScrollArea;
class QVBoxLayout;

struct Setting;

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    explicit MainWindow();
    virtual ~MainWindow();
    bool init ( const char *SettingFile );
public slots:
    void checkNotification();
    void showNotificationWithNotify();
    void showNotificationWithoutNotify();
    void showAbout();
    void changeCheckInterval();
protected:
    void closeEvent ( QCloseEvent *event ) override;
private:
    void createMenus();
    void authorize ( ) ;
    void showNotification ( bool notify = true );
    QByteArray pin_dialog();
    Setting *setting;
    API *api;
    QVBoxLayout *main_layout;
    QSystemTrayIcon tray;
    QTimer timer;
    QByteArrayList hash_list;
    const unsigned int max_list_size = 20;//メインウィンドウに表示する最大の通知数
};
