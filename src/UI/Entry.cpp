/*
 * Copyright 2017 PG_MANA
 *
 * This software is Licensed under the Apache License Version 2.0
 * See LICENSE.md
 *
 * メインウィンドウに並べて表示するやつ
 */
#include "Entry.h"
#include "../API/Parser.h"
#include <QtWidgets>

Entry::Entry ( const NotificationData& nfdata ) :data ( nfdata ) {
    QVBoxLayout *main_layout = new QVBoxLayout ( this );

    QLabel *title = new QLabel ( data.title );
    title->setWordWrap ( true );
    title->setStyleSheet ( "font-weight:bold;font-size:large;" );
    main_layout->addWidget ( title );

    QLabel *author = new QLabel ( tr ( "投稿:" )+data.author );
    author->setWordWrap ( true );
    main_layout->addWidget ( author );

    QLabel *text = new QLabel ( QString ( data.text ).replace ( "\n","<br />" ) );
    text->setWordWrap ( true );
    text->setTextFormat ( Qt::RichText );
    main_layout->addWidget ( text );

    QFrame *frame = new QFrame;
    frame->setFrameShape ( QFrame::HLine );
    main_layout->addWidget ( frame );
}

/*
 * 引数:なし
 * 戻値:保存されているURL
 * 概要:保存されているURLを返す。
 */
QString Entry::url() {
    return data.link;
}

/*
 * 引数:QMouseEvent
 * 戻値:なし
 * 概要:ダブルクリックされたときに呼ばれる。該当イベントをブラウザで表示する。
 */
void Entry::mouseDoubleClickEvent ( QMouseEvent* event ) {
    QDesktopServices::openUrl ( QUrl ( data.link ) );
    event->accept();
    return;
}
