/*
 * Copyright 2017 PG_MANA
 *
 * This software is Licensed under the Apache License Version 2.0
 * See LICENSE.md
 *
 * メインウィンドウ クラス
 * 主役である画面の表示を行う。
 */
#include "MainWindow.h"
#include "Entry.h"
#include "../API/API.h"
#include "../API/Setting.h"
#include "../API/Parser.h"
#include "../Sardine.h"
#include <QtWidgets>
#include <QNetworkReply>
#include <QCryptographicHash>

MainWindow::MainWindow() : tray ( qApp->windowIcon() ) {

    //メイン表示
    QScrollArea *timeline = new QScrollArea;

    QWidget *tl_main = new QWidget;
    main_layout = new QVBoxLayout ( tl_main );
    timeline->setWidgetResizable ( true );
    timeline->setWidget ( tl_main );
    timeline->setFrameShape ( QFrame::NoFrame ); //枠線をなくす
    setCentralWidget ( timeline );

    createMenus();
}

MainWindow::~MainWindow() {
    tray.hide();
}

/*
 * 引数:event
 * 戻値:なし
 * 概要:ウィンドウを閉じる時に呼ばれる。ウィンドウ位置の保存を行う。
 */
void MainWindow::closeEvent ( QCloseEvent *event ) {
    //ウィンドウ状態保存
    setting->setGeometry ( saveGeometry() );
    QMainWindow::closeEvent ( event );
    return;
}


/*
 * 引数:setting_file(設定ファイルのパス)
 * 戻値:成功はtrue、失敗はfalse
 * 概要:渡された設定ファイルから設定を読み込み、基本設定を行う。初期化のあとに呼ぶ。
 */
bool MainWindow::init ( const char *setting_file ) {
    //設定読み込み
    setting = new Setting ( setting_file );

    if ( setting->isEmpty() ) {
        //初期設定
        api = nullptr;
        try {
            ( QMessageBox::question ( this,APP_NAME,tr ( "アプリ連携の認証を行いますか。" ),QMessageBox::Yes | QMessageBox::No,QMessageBox::Yes ) ==QMessageBox::Yes ) ?authorize () /*エラーはthrowされて下に行く*/:throw tr ( "アプリ連携を行わなければこのソフトウェアは使用できません。" );
        } catch ( QString &error ) {
            //delete api;<=~MainWindowで呼ばれる。
            QMessageBox::critical ( this,APP_NAME,error );
            return false;
        }
    } else {
        api = new API ( setting );
        restoreGeometry ( setting->geometry() );
    }

    //APIが正しく動作する保証ができたので準備する
    //通知表示
    connect ( api->notification_v2 (),&QNetworkReply::finished,this,&MainWindow::showNotificationWithoutNotify );
    //Timer設定
    connect ( &timer,&QTimer::timeout,this,&MainWindow::checkNotification );
    timer.setInterval ( 5 * 60 * 1000 ); //3分
    timer.start();
    //Tray通知
    tray.show();
    QMenu * tray_menu = new QMenu;
    tray_menu->setToolTipsVisible ( true );
    tray_menu->addAction ( style()->standardIcon ( QStyle::SP_BrowserReload ),tr ( "新着情報をチェックする(&R)" ),this,&MainWindow::checkNotification )->setToolTip ( tr ( "未確認の新着情報がないか確認します。" ) );
    tray_menu->addAction ( style()->standardIcon ( QStyle::SP_TitleBarCloseButton ),tr ( "終了(&Q)" ),qApp,&QApplication::exit )->setToolTip ( tr ( "すべてのウィンドウを閉じ、アプリケーションを終了します。" ) );
    tray.setContextMenu ( tray_menu );
    connect ( &tray,&QSystemTrayIcon::activated,this,&MainWindow::show );
    connect ( &tray,&QSystemTrayIcon:: messageClicked,this,&MainWindow::show );
    return true;
}

/*
 * 引数:なし
 * 戻値:なし
 * 概要:メニューバーを作って設定する。ウィンドウ初期化用。
 */
void MainWindow::createMenus() {
    menuBar()->setNativeMenuBar ( true );

    //設定
    QMenu *setting_menu = menuBar()->addMenu ( tr ( "設定(&S)" ) );
    setting_menu->setToolTipsVisible ( true );
    setting_menu->addAction ( style()->standardIcon ( QStyle::SP_BrowserReload ),tr ( "新着情報をチェックする(&R)" ),this,&MainWindow::checkNotification )->setToolTip ( tr ( "未確認の新着情報がないか確認します。" ) );
    setting_menu->addAction ( tr ( "更新間隔を設定(&I)" ),this,&MainWindow::changeCheckInterval )->setToolTip ( tr ( "新着情報をチェックする間隔を設定します。" ) );
    setting_menu->addAction ( style()->standardIcon ( QStyle::SP_TitleBarCloseButton ),tr ( "終了(&E)" ),qApp,&QApplication::exit )->setToolTip ( tr ( "すべてのウィンドウを閉じ、アプリケーションを終了します。" ) );
    //ヘルプ
    QMenu *help_menu = menuBar()->addMenu ( tr ( "ヘルプ(&H)" ) );
    help_menu->setToolTipsVisible ( true );
    help_menu->addAction ( QIcon ( ":/icon-normal.png" ),tr ( APP_NAME "について(&A)" ),this,&MainWindow::showAbout )->setToolTip ( tr ( "バージョンやライセンスについてのダイアログを表示します。" ) );
    help_menu->addAction ( style()->standardIcon ( QStyle::SP_TitleBarMenuButton ),tr ( "Qtについて(&Q)" ),qApp,&QApplication::aboutQt )->setToolTip ( tr ( "使用されているQtのライブラリのバージョンやライセンスについてのダイアログを表示します。" ) );

    return;
}

/*
 * 引数:なし
 * 戻値:なし
 * 概要:APIアプリ連携の認証を行う。それ以外では呼ばない。同期処理を行う。
 */
void MainWindow::authorize ( ) {
    /*throwしまくりである...(throw時のapiの削除は他でするので気にしなくてよい*/
    QEventLoop event;//ここだけ同期処理
    QNetworkReply *rep;
    QList<QByteArray> result;
    QByteArray token_key,data,pincode;

    api =new API;
    rep=api->request_token();
    connect ( rep,&QNetworkReply::finished,&event,&QEventLoop::quit );
    event.exec();
    if ( rep->error() != QNetworkReply::NoError ) {
        delete rep;
        throw tr ( "リクエストトークンの取得に失敗しました。" );
    }
    result =rep->readAll().split ( '&' );
    delete rep;

    token_key = result[0].split ( '=' ) [1];
    if ( !QDesktopServices::openUrl ( api->authorize_url ( token_key ) ) ) throw tr ( "ブラウザの起動に失敗しました。" );
    pincode = pin_dialog();//PIN入力
    if ( pincode.isEmpty() ) throw tr ( "認証がキャンセルされました。" );
    rep = api->access_token ( token_key.constData(),result[1].split ( '=' ) [1].constData(),pincode.constData() );
    connect ( rep,&QNetworkReply::finished,&event,&QEventLoop::quit );
    event.exec();
    if ( rep->error() != QNetworkReply::NoError ) {
        delete rep;
        throw tr ( "アクセストークンの取得に失敗しました。" );
    }
    data =rep->readAll();
    delete rep;
    api->decode_access_token ( data,*setting );
    if ( !setting->save ( ) ) throw tr ( "設定の保存に失敗しました。" );
    return;
}

/*
 * 引数:なし
 * 戻値:pin(入力された文字列)
 * 概要:PIN入力ダイアログを作って表示して結果を返す。
 */
QByteArray MainWindow::pin_dialog() {
    QDialog dialog;
    QVBoxLayout *layout =new QVBoxLayout ( &dialog );
    QLineEdit *pincode_editer =new QLineEdit;
    QPushButton *ok_button = new QPushButton ( tr ( "OK" ) );

    connect ( ok_button,&QPushButton::clicked,&dialog,&QWidget::close );
    pincode_editer->setMaxLength ( 7 ); //最大7文字
    layout->addWidget ( new QLabel ( tr ( "表示されたブラウザでCybozuLiveの認証して、表示されたPINコードを入力してください。" ) ) );
    layout->addWidget ( pincode_editer );
    layout->addWidget ( ok_button );
    dialog.exec();
    return pincode_editer->text().toLatin1();
}

/*
 * 引数:なし
 * 戻値:なし
 * 概要:バージョン情報を表示するダイアログを出す。
 */
void MainWindow::showAbout() {
    QMessageBox::about ( this,tr ( APP_NAME "について" ),
                         tr ( "<b>" APP_NAME_LONG "</b>"
                              "<p>Ver " APP_VERSION "</p>"
                              "<p>Qtを使用して製作されているサイボウズLive更新情報受信クライアント。</p>"
                              "<p>本ソフトウェアはQtオープンソース版のLGPLv3を選択しています。詳しくは<a href=\"https://www.qt.io/licensing/\">https://www.qt.io/licensing/</a>をご覧ください。</p>"
                              "<b>License</b>"
                              "<p>" APP_COPYRIGHT  "<br /><br />"
                              "Licensed under the Apache License, Version 2.0 (the \"License\");<br />"
                              "you may not use this file except in compliance with the License.<br />"
                              "You may obtain a copy of the License at<br /><br />"
                              "<a href=\"https://www.apache.org/licenses/LICENSE-2.0\" >https://www.apache.org/licenses/LICENSE-2.0</a><br />"
                              "Unless required by applicable law or agreed to in writing, software"
                              "distributed under the License is distributed on an \"AS IS\" BASIS,"
                              "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.<br />"
                              "See the License for the specific language governing permissions and"
                              "limitations under the License.</p>"
                              "<a href=\"" APP_HOMEPAGE "\">このソフトウェアについてのページを開く</a>") );
    return;
}

/*
 * 引数:なし
 * 戻値:なし
 * 概要:新着情報を確認する間隔を変更
 */
void MainWindow::changeCheckInterval() {
    int itv = QInputDialog::getInt ( this,APP_NAME,tr ( "新着情報を受信する間隔を分単位で指定してください。" ), setting->interval(),5 );
    timer.setInterval ( itv );
    setting->setInterval ( itv );
    return;
}

/*
 * 引数:なし
 * 戻値:なし
 * 概要:新着情報を確認する
 */
void MainWindow::checkNotification() {
    connect ( api->notification_v2 ( true ),&QNetworkReply::finished,this,&MainWindow::showNotificationWithNotify );
    return;
}

/*
 * 引数:なし
 * 戻値:なし
 * 概要:画面に追加し通知を表示する。slot用
 */
void MainWindow::showNotificationWithNotify() {
    return showNotification ( true );
}

/*
 * 引数:なし
 * 戻値:なし
 * 概要:画面に追加し通知は表示しない。slot用
 */
void MainWindow::showNotificationWithoutNotify() {
    return showNotification ( false );
}


/*
 * 引数:notify(通知を表示するかどうか)
 * 戻値:なし
 * 概要:画面に追加しnotify=trueのとき通知を表示する。
 */
void MainWindow::showNotification ( bool notify ) {
    Parser parser ( qobject_cast<QNetworkReply*> ( sender() )->readAll() );
    auto result = parser.getNotification( );

    for ( NotificationData &data:result ) {
        QByteArray &&hash = QCryptographicHash::hash ( data.link.toUtf8(),QCryptographicHash::Md5 );//Md5はセキュリティ的に問題があるが、衝突に対して大きなデメリットはないしこれが計算が速くビット長が短い。
        if ( hash_list.contains ( hash ) ) continue;
        //古いものを削除
        if ( main_layout->count() >= max_list_size ) {
            QLayoutItem *old = main_layout->takeAt ( 0 ); //一番最初に追加したやつから番号が振られる。
            if ( old ) {
                delete old->widget();
                hash_list.removeAt ( 0 );
            }
        }
        main_layout->addWidget ( new Entry ( data ) );
        if ( notify ) tray.showMessage ( data.author,data.title+"\n"+data.text.left ( 20 ) );
        hash_list.append ( hash );
    }
    return;
}
