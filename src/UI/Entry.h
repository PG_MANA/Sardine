/*
 * Copyright 2017 PG_MANA
 *
 * This software is Licensed under the Apache License Version 2.0
 * See LICENSE.md
 *
 * メインウィンドウに並べて表示するやつ
 */
#pragma once

#include <QWidget>
#include "../API/Parser.h"

class Entry : public QWidget {
    Q_OBJECT
public:
    explicit Entry ( const NotificationData &data );
    QString url();
protected:
    void mouseDoubleClickEvent ( QMouseEvent *event ) override;
private:
    NotificationData data;
};
