/*
 * Copyright 2017 PG_MANA
 *
 * This software is Licensed under the Apache License Version 2.0
 * See LICENSE.md
 *
 * OAuth 1.0a クラス
 * QtにOAuthあるが実装してしまったのでこれを使う。
 */
#include "OAuth.h"
#include "SHA1cc.h"
#include <QDateTime>
#include <QUrl>
#include <string.h>

OAuth::OAuth ( const char *_cons_key, const char *_cons_sec, const char *_oauth_token,const  char *_oauth_sec ) {
    //アプリ認証する前はoauth_keyなどが空だから、認証後はこれを呼ぶ。
    sprintf ( Key, "%s&%s", _cons_sec, _oauth_sec ); //こっちが簡単。
    strcpy ( cons_key, _cons_key );
    strcpy ( cons_sec, _cons_sec );
    strcpy ( oauth_token, _oauth_token );
    strcpy ( oauth_secret, _oauth_sec );
}

OAuth::OAuth ( const OAuth &other ) {
    //ポインタを保持してないためmemcpyでもいい
    strcpy ( Key,other.Key );
    strcpy ( cons_key,other.cons_key );
    strcpy ( cons_sec,other.cons_sec );
    strcpy ( oauth_token,other.oauth_token );
    strcpy ( oauth_secret,other.oauth_secret );
}

/*
 * 引数:url(アクセスするURL),post(POSTアクセスの場合はtrue,GETアクセスならfalse),elements(OAuthエントリー),OAuth_str(結果受け取り用)
 * 戻値:なし
 * 概要:渡された引数を使ってOAuthヘッダを作成する。
 */
void OAuth::makeOAuthHeader ( const char *url, bool post, std::vector<OAuth::entry>&elements, std::string &OAuth_str ) {
    QByteArray    OAuth_sig_str,timestamp,nonce,signature;
    char                   buf[Buff_Size];

    //下準備
#if(QT_VERSION>=QT_VERSION_CHECK(5,8,0))
    timestamp = QString::number ( QDateTime::currentDateTimeUtc().toSecsSinceEpoch() ).toLatin1(); //Qt5.8以降
#else
    //time_tはulonglong だから2038年問題には強そうだが、Qt 5.8が使えるようになり次第、この行は廃止する。
    timestamp = QString::number ( QDateTime::currentDateTimeUtc().toTime_t() ).toLatin1() /*数字だけのはず*/;
#endif
    nonce = QString::number ( QTime::currentTime().msec() ).toLatin1(); //qrand使わんでもこれで事足りそう。

    //とりあえず必要項目をプッシュする。
    elements.push_back ( { "oauth_consumer_key",cons_key ,false } ); //変数名指定してない
    elements.push_back ( { "oauth_timestamp", timestamp.constData(),false } );
    elements.push_back ( { "oauth_nonce", nonce.constData() ,false } );
    elements.push_back ( { "oauth_signature_method","HMAC-SHA1",false } );
    if ( *oauth_token != '\0' ) elements.push_back ( { "oauth_token",oauth_token,false } );
    elements.push_back ( { "oauth_version","1.0",false } );

    //名前でソート(速度とメモリ使用量が...)
    std::sort ( elements.begin(), elements.end(), [] ( entry e1, entry e2 ) {
        return strcmp ( e1.title, e2.title ) <= 0;
    } );
    OAuth_sig_str.reserve ( elements.capacity() );
    //シグネチャ生成
    for ( OAuth::entry &element : elements ) {
        OAuth_sig_str += ( ( OAuth_sig_str.isEmpty() ) ?"":"&" ) + QByteArray ( element.title ).replace ( ',',"%2C" )  + "=" + QByteArray ( element.body ).replace ( ',',"%2C" ); //メモリはどうなのだろうか...(追記:OAuth::entryのbodyはQByteArrayにしても良さそう)
    }

    OAuth_sig_str = ( post ? "POST&" : "GET&" ) + QUrl::toPercentEncoding ( url ) + "&" + QUrl::toPercentEncoding ( OAuth_sig_str );

    //鍵はあるはず。
    HMAC_SHA1 ( Key, OAuth_sig_str.constData(), buf );
    signature = QUrl::toPercentEncoding ( QByteArray::fromRawData ( buf,Buff_Size ).toBase64() ); //めんどい処理...

    //ようやく本体の生成。
    elements.insert (
    std::find_if ( elements.begin(), elements.end(), [] ( entry e ) {
        return strcmp ( e.title,"oauth_signature" ) > 0;
    } ),
    { "oauth_signature", signature.constData(),false } );
    //いちいちいらないelementsを消してても時間がかかるので削除しないようにして無視するようにした。
    OAuth_str.reserve ( elements.capacity() );
    //OAuth_str = "OAuth ";//ここもWin版と違う。
    for ( OAuth::entry &element : elements ) {
        if ( element.del_when_header ) continue;
        OAuth_str += ( ( OAuth_str.empty() ) ?"OAuth "/*最初に追加する文字列*/:"," )+std::string ( element.title ) + "=\"" + std::string ( element.body ) + "\"";
    }
    //OAuth_str += " \r\n"; ここはWin版と違う。
    return;
}

/*
 * 引数:key(鍵、終端に\0をつける),data(計算するデータ、終端に\0をつける),buff(結果受け取り用)
 * 戻値:なし
 * 概要:渡された引数を使ってHMAC-SHA1を算出する。自前だから間違った実装な可能性あり。
 */
void OAuth::HMAC_SHA1 ( const char *key, const char *data, char buff[Buff_Size] ) {
    SHA1_Context_t t;
    uint8_t SHA1_key[Block_Size];
    size_t  data_size = strlen ( data ),
            key_size = strlen ( key ),
            cnt/*なんとなく型合わせるために*/;

    if ( key_size > Block_Size ) { //Block_Sizeを超えたら収まるようにキーを縮める(ハッシュを出す)らしい
        SHA1cc_Init ( &t );
        SHA1cc_Update ( &t, key, key_size );
        SHA1cc_Finalize ( &t, SHA1_key );
        key_size = Buff_Size;//SHA1にかけると20Byteになる
    } else strcpy ( ( char * ) SHA1_key, key );

    SHA1cc_Init ( &t );
    //KeyでXORするらしい
    for ( cnt = 0; cnt < key_size; cnt++ ) * ( SHA1_key + cnt ) ^= 0x36;
    for ( ; cnt < Block_Size; cnt++ ) * ( SHA1_key + cnt ) = 0x36; //Keyが足りなかった分
    SHA1cc_Update ( &t, SHA1_key, Block_Size );
    SHA1cc_Update ( &t, data, data_size );
    SHA1cc_Finalize ( &t, ( uint8_t * ) buff );

    //おんなじことを0x5cでもするらしい
    SHA1cc_Init ( &t );
    //KeyでXORするらしい
    //for (cnt = 0; cnt < key_size; cnt++) *(SHA1_key+cnt)=(*(SHA1_key + cnt)^0x36/*再反転して元に戻す*/) ^ 0x5C;
    for ( cnt = 0; cnt < key_size; cnt++ ) * ( SHA1_key + cnt ) ^= 0x6A; //^0x36と^0x5Cをまとめたもの
    for ( ; cnt < Block_Size; cnt++ ) * ( SHA1_key + cnt ) = 0x5C; //Keyが足りなかった分
    SHA1cc_Update ( &t, SHA1_key, Block_Size );
    SHA1cc_Update ( &t, ( uint8_t * ) buff, Buff_Size );
    SHA1cc_Finalize ( &t, ( uint8_t * ) buff );
    return;
}


