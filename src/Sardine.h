/*
 * Copyright 2017 PG_MANA
 * 
 * This software is Licensed under the Apache License Version 2.0 
 * See LICENSE.md
 * 
 * Sardine.h
 * 全体設定
 */
#pragma once

//設定
#define USER_AGENT           "Sardine"
#define APP_NAME             "Sardine"
#define APP_NAME_LONG   "CybozuLive Client Sardine"
#define APP_VERSION          "0.0.3"
#define APP_HOMEPAGE    "https://bitbucket.org/PG_MANA/sardine/"
#define APP_COPYRIGHT        "Copyright 2017 PG_MANA"
